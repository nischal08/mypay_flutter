## 0.0.1

- Initial release of the mypay_flutter package.
## 0.0.5

- Documentation updated.
## 1.0.0

- Resolved the navigation issue after transaction completion.
## 1.0.1

- Documentation updated.
