import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mypay_flutter/mypay_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MyPay Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const MyPayApp(title: 'MyPay Payment'),
    );
  }
}

class MyPayApp extends StatefulWidget {
  const MyPayApp({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyPayApp> createState() => _MyPayAppState();
}

class _MyPayAppState extends State<MyPayApp> {
  String message = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Example Using mypay button - 1
            MyPayButton(
              apiKey: "API-KEY",
              paymentConfig: MyPayConfig(
                  amount: 2000,
                  environment: MyPayEnvironment.dev,
                  orderId: "QWE-SEW-SFES-QXZS",
                  merchantId: "CAFEQ",
                  userName: "username1",
                  password: "decryptedPassword",
                  returnUrl: "https://mypay.com.np"),
              width: 100,
              onFailure: (result) async {
                message = result.remarks;
                if (kDebugMode) {
                  print("Transaction Failed");
                }
              },
              onSuccess: (result) async {
                message = result.remarks;
                if (kDebugMode) {
                  print("Transaction Successful");
                }
              },
              onCancel: (result) {
                message = result!.remarks;
                if (kDebugMode) {
                  print("Transaction Cancelled");
                }
              },
            ),
            const SizedBox(
              height: 16,
            ),

            /// Example Using pay function mannaully - 2
            TextButton(
              onPressed: () async {
                MyPay.payment(
                  context,
                  config: MyPayConfig(
                      amount: 2000,
                      environment: MyPayEnvironment.dev,
                      orderId: "QWE-SEW-SFES-QXZS",
                      merchantId: "CAFEQ",
                      userName: "username1",
                      password: "decryptedPassword",
                      returnUrl: "https://mypay.com.np"),
                  apiKey: "API-KEY",
                  onFailure: (result) async {
                    message = result.remarks;
                    setState(() {});
                    if (kDebugMode) {
                      print("Transaction Failed");
                    }
                  },
                  onSuccess: (result) async {
                    message = result.remarks;
                    if (kDebugMode) {
                      print("Transaction Successful");
                    }
                  },
                  onCancel: (result) {
                    message = result.remarks;
                    if (kDebugMode) {
                      print("Transaction Cancelled");
                    }
                  },
                );
              },
              child: const Text('Pay with MyPay'),
            ),
            const SizedBox(
              height: 16,
            ),
            if (message.isNotEmpty)
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 60),
                child: Text('Console: Payment Info, Message: $message'),
              ),
          ],
        ),
      ),
    );
  }
}
