part of mypay_flutter;
class MyPayBaseStatusModel {
  MyPayBaseStatusModel({
    required this.merchantTransactionId,
    required this.memberContactNumber,
    required this.netAmount,
    required this.status,
    required this.remarks,
    required this.gatewayTransactionId,
    required this.orderId,
    required this.message,
    required this.responseMessage,
    required this.details,
    required this.reponseCode,
  });
  late final String merchantTransactionId;
  late final String memberContactNumber;
  late final double netAmount;
  late final int status;
  late final String remarks;
  late final String gatewayTransactionId;
  late final String orderId;
  late final String message;
  late final String responseMessage;
  late final String details;
  late final int reponseCode;

  MyPayBaseStatusModel.fromJson(Map<String, dynamic> json) {
    merchantTransactionId = json['MerchantTransactionId'];
    memberContactNumber = json['MemberContactNumber'];
    netAmount = json['NetAmount'];
    status = json['Status'];
    remarks = json['Remarks'];
    gatewayTransactionId = json['GatewayTransactionId'];
    orderId = json['OrderId'];
    message = json['Message'];
    responseMessage = json['responseMessage'];
    details = json['Details'];
    reponseCode = json['ReponseCode'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['MerchantTransactionId'] = merchantTransactionId;
    data['MemberContactNumber'] = memberContactNumber;
    data['NetAmount'] = netAmount;
    data['Status'] = status;
    data['Remarks'] = remarks;
    data['GatewayTransactionId'] = gatewayTransactionId;
    data['OrderId'] = orderId;
    data['Message'] = message;
    data['responseMessage'] = responseMessage;
    data['Details'] = details;
    data['ReponseCode'] = reponseCode;
    return data;
  }
}
