part of mypay_flutter;

class MyPayConfig {
  MyPayConfig({
    required this.amount,
    required this.environment,
    required this.orderId,
    required this.merchantId,
    required this.password,
    required this.userName,
    this.returnUrl = "https://mypay.com.np",
  });

  /// The payment [amount] in paisa.
  final double amount;

  /// The merchant username
  final String userName;

  /// The merchant password
  final String password;

  /// The merchant indentity
  final String merchantId;

  /// The order indentity
  final String orderId;

  /// The payment return callback Url
  final String returnUrl;

  /// An environment of the development or production
  final MyPayEnvironment environment;
}

enum MyPayEnvironment { dev, live }
