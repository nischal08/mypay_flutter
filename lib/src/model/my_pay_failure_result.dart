part of mypay_flutter;

class MyPayFailureResult extends MyPayBaseStatusModel {
  MyPayFailureResult({
    String merchantTransactionId = "",
    String memberContactNumber = "",
    required double netAmount,
    int status = 0,
    required String remarks,
    String gatewayTransactionId = "",
    String orderId = "",
    String message = "",
    String responseMessage = "",
    String details = "",
    int reponseCode = 0,
  }) : super(
            status: status,
            details: details,
            gatewayTransactionId: gatewayTransactionId,
            memberContactNumber: memberContactNumber,
            merchantTransactionId: merchantTransactionId,
            netAmount: netAmount,
            message: message,
            orderId: orderId,
            remarks: remarks,
            reponseCode: reponseCode,
            responseMessage: responseMessage);

  MyPayFailureResult.fromParent(MyPayBaseStatusModel parent)
      : super(
            status: parent.status,
            details: parent.details,
            gatewayTransactionId: parent.gatewayTransactionId,
            memberContactNumber: parent.memberContactNumber,
            merchantTransactionId: parent.merchantTransactionId,
            netAmount: parent.netAmount,
            message: parent.message,
            orderId: parent.orderId,
            remarks: parent.remarks,
            reponseCode: parent.reponseCode,
            responseMessage: parent.responseMessage);
}
