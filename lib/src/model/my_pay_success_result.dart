part of mypay_flutter;

class MyPaySuccessResult extends MyPayBaseStatusModel {
  MyPaySuccessResult({
    required String merchantTransactionId,
    required String memberContactNumber,
    required double netAmount,
    required int status,
    required String remarks,
    required String gatewayTransactionId,
    required String orderId,
    required String message,
    required String responseMessage,
    required String details,
    required int reponseCode,
  }) : super(
            status: status,
            details: details,
            gatewayTransactionId: gatewayTransactionId,
            memberContactNumber: memberContactNumber,
            merchantTransactionId: merchantTransactionId,
            netAmount: netAmount,
            message: message,
            orderId: orderId,
            remarks: remarks,
            reponseCode: reponseCode,
            responseMessage: responseMessage);

  MyPaySuccessResult.fromParent(MyPayBaseStatusModel parent)
      : super(
            status: parent.status,
            details: parent.details,
            gatewayTransactionId: parent.gatewayTransactionId,
            memberContactNumber: parent.memberContactNumber,
            merchantTransactionId: parent.merchantTransactionId,
            netAmount: parent.netAmount,
            message: parent.message,
            orderId: parent.orderId,
            remarks: parent.remarks,
            reponseCode: parent.reponseCode,
            responseMessage: parent.responseMessage);
}
