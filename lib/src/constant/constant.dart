const String kMyPayDevUrl = "https://testapi.mypay.com.np";
const String kMyPayLiveUrl = "https://smartdigitalnepal.com";
const String kMyPayPaymentLiveUrl = "$kMyPayLiveUrl/api/use-mypay-payments";
const String kMyPayPaymentDevUrl = "$kMyPayDevUrl/api/use-mypay-payments";
const String kMyPayStatusLiveUrl =
    "$kMyPayLiveUrl/api/use-mypay-payments-status";
const String kMyPayStatusDevUrl = "$kMyPayDevUrl/api/use-mypay-payments-status";