part of mypay_flutter;

class MyPayButton extends StatelessWidget {
  /// button width
  final double? width;

  /// button height
  final double? height;

  /// button background color
  final Color? color;

  /// button border color
  final Color? borderColor;

  /// onSuccess callback, when payment will be succeed this call back will be called with MyPay payment response
  final Function(MyPaySuccessResult) onSuccess;

  /// onFailure callback, when payment will be failed this call back will be called with a fail message
  final Function(MyPayFailureResult) onFailure;

  /// onConcel callback, when payment will be canceled by the user this call back will be called
  final Function(MyPayCancelResult?) onCancel;

  /// MyPay Payment Configuration object , required to initialize payment screen
  final MyPayConfig paymentConfig;

  /// button borderRadius
  final double? radius;

  /// optional widget in place of title Text field, if use wants to place a row with MyPay icon or different widget
  final Widget? widget;

  /// button title [default] tp Pay with MyPay
  final String? title;

  /// MyPay api key
  final String apiKey;

  /// button title's textStyle
  final TextStyle? textStyle;

  const MyPayButton({
    Key? key,
    this.radius,
    this.width,
    this.textStyle,
    this.widget,
    this.borderColor,
    this.color,
    this.height,
    required this.paymentConfig,
    required this.onSuccess,
    required this.onFailure,
    required this.onCancel,
    required this.apiKey,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 40.0,
      child: ElevatedButton(
        onPressed: () async {
          MyPay.payment(context,
              config: paymentConfig,
              apiKey: apiKey,
              onSuccess: onSuccess,
              onCancel: onCancel,
              onFailure: onFailure);
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: color,
          elevation: 0,
          side: borderColor != null
              ? BorderSide(
                  width: 1,
                  color: borderColor!,
                )
              : null,
          minimumSize: Size(width ?? double.infinity, height ?? 40.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radius ?? 4)),
          ),
        ),
        child: widget ?? Text(title ?? 'Pay with MyPay', style: textStyle),
      ),
    );
  }
}
