part of mypay_flutter;

class MyPayPaymentScreen extends StatefulWidget {
  final String url;
  final String decryptedApiKey;

  /// Page appbar
  final AppBar? appBar;
  const MyPayPaymentScreen({
    super.key,
    required this.url,
    required this.decryptedApiKey,
    this.appBar,
  });

  @override
  State<MyPayPaymentScreen> createState() => _MyPayPaymentScreenState();
}

class _MyPayPaymentScreenState extends State<MyPayPaymentScreen> {
  late InAppWebViewController webView;

  bool showProgress = true;
  bool isInit = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: widget.appBar ??
          AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: const Color(0xfffd7e14),
            title: const Text("Pay Via MyPay"),
            actions: [
              GestureDetector(
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    size: 30,
                    Icons.close,
                  ),
                ),
              )
            ],
          ),
      body: Column(
        children: [
          Expanded(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: InAppWebView(
                    initialOptions: InAppWebViewGroupOptions(
                        crossPlatform:
                            InAppWebViewOptions(transparentBackground: true)),
                    initialUrlRequest: URLRequest(url: Uri.parse(widget.url)),
                    onWebViewCreated: (InAppWebViewController controller) {
                      webView = controller;
                    },
                    onLoadStart: (controller, url) {
                      controller.getUrl().then((value) async {
                        log(value.toString(), name: "onLoadStart");
                        if (value
                                .toString()
                                .contains("MerchantTransactionId")) {
                          Navigator.pop(context,
                              value?.queryParameters["MerchantTransactionId"]);
                        }
                      });
                    },
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {
                      if (isInit && progress > 50.0) {
                        setState(() {
                          showProgress = false;
                        });
                        isInit = false;
                      }
                    },
                  ),
                ),
                if (showProgress)
                  Align(
                      alignment: Alignment.center, child: _buildProgressBar()),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildProgressBar() {
    return const CircularProgressIndicator();
    // You can use LinearProgressIndicator also
    //  return LinearProgressIndicator(
    //    value: progress,
    //    valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange),
    //    backgroundColor: Colors.blue,
    //  );
  }
}
