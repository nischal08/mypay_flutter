part of mypay_flutter;

class MyPay {
  static payment(
    BuildContext context, {
    required MyPayConfig config,
    AppBar? appBar,
    required String apiKey,
    required Function(MyPaySuccessResult success) onSuccess,
    required Function(MyPayCancelResult cancel) onCancel,
    required Function(MyPayFailureResult failure) onFailure,
  }) async {
    late final String? merchantTransactionId;
    final body = {
      "Amount": config.amount,
      "OrderId": config.orderId,
      "UserName": config.userName,
      "Password": config.password,
      "MerchantId": config.merchantId,
      "ReturnUrl": config.returnUrl
    };
    final header = {
      "API_KEY": apiKey,
      "Content-Type": "application/json",
    };
    try {
      Response response = await Dio().post(
        config.environment == MyPayEnvironment.live
            ? kMyPayPaymentLiveUrl
            : kMyPayPaymentDevUrl,
        data: body,
        options: Options(headers: header),
      );
      if (context.mounted) {
        merchantTransactionId = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MyPayPaymentScreen(
                appBar: appBar,
                decryptedApiKey: apiKey,
                url: response.data["RedirectURL"]),
          ),
        );
      }
      if (merchantTransactionId == null) {
        onCancel(MyPayCancelResult(
            netAmount: config.amount, remarks: "Transaction Cancelled"));
        return;
      }
      final responseData = await Dio().post(
        config.environment == MyPayEnvironment.live
            ? kMyPayStatusLiveUrl
            : kMyPayStatusDevUrl,
        data: {
          "MerchantTransactionId": merchantTransactionId,
        },
        options: Options(
            headers: {"Content-Type": "application/json", "API_KEY": apiKey}),
      );
      MyPayBaseStatusModel myPayStatusModel =
          MyPayBaseStatusModel.fromJson(responseData.data);
      switch (myPayStatusModel.status) {
        case 1:
          onSuccess(MyPaySuccessResult.fromParent(myPayStatusModel));
          break;
        case 3:
          onCancel(MyPayCancelResult.fromParent(myPayStatusModel));
          break;
        default:
          onFailure(MyPayFailureResult.fromParent(myPayStatusModel));
      }
    } on DioError catch (ex) {
      debugPrint(ex.toString());
      onFailure(
        MyPayFailureResult(
          netAmount: config.amount,
          remarks: "Invalid mypay config credential or Server error.",
        ),
      );
    }
  }
}
