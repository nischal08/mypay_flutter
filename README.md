# MyPay Flutter [![Share on Twitter](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/intent/tweet?text=Flutter%20MyPayPayment%20plugin!&url=https://gitlab.com/nischal08/mypay_flutter&hashtags=flutter,flutterio,dart,wallet,MyPay,paymentgateway) [![Share on Facebook](https://img.shields.io/badge/share-facebook-blue.svg?longCache=true&style=flat&colorB=%234267b2)](https://www.facebook.com/sharer/sharer.php?u=https%3A//gitlab.com/nischal08/mypay_flutter)

[![Pub Version](https://img.shields.io/pub/v/mypay_flutter.svg)](https://pub.dev/packages/mypay_flutter)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Introducing an unofficial Flutter plugin designed for seamless integration with the MyPay Payment Gateway. This plugin empowers you to effortlessly incorporate the MyPay Payment Gateway into your Flutter application, enabling you to receive payments from your valued customers. Whether you're developing an eCommerce platform or any other application that necessitates secure transactions, this plugin simplifies and streamlines the integration process, ensuring a hassle-free experience.

![Cover Image](https://gitlab.com/nischal08/mypay_flutter/-/raw/main/raw/banner.png)

# Note

This package does not rely on any plugins or native APIs for payment initialization. Instead, it leverages the Flutter InAppWebView package. A special mention and appreciation to the developer of the [InAppWebView](https://pub.dev/packages/flutter_inappwebview) package for offering this valuable resource.

## Features

- Seamless integration
- Simplified setup
- Pure Dart implementation
- User-friendly interface

## Requirements

- Android: `minSdkVersion 17` and add support for `androidx` (see [AndroidX Migration](https://flutter.dev/docs/development/androidx-migration))
- iOS: `--ios-language swift`, Xcode version `>= 11`

## Setup

### iOS

No Configuration Needed

For more info, [see here](https://pub.dev/packages/flutter_inappwebview#important-note-for-ios)

### Android

Set `minSdkVersion` of your `android/app/build.gradle` file to at least 17.

For more info, [see here](https://pub.dev/packages/flutter_inappwebview#important-note-for-android)

# Usage

1. Add `mypay_flutter` as a dependency in your `pubspec.yaml` file:

```yaml
dependencies:
  mypay_flutter: ^1.0.1
```

2. Import the package in your Dart code:

`import 'package:mypay_flutter/mypay_flutter.dart';`

3. Create an instance of `MyPayConfig` with your payment information:

The MyPayConfig class holds the configuration details for the payment gateway. Pass an instance of
MyPayConfig to the payment() method of the MyPay class to initiate the payment process.

```
final config = MyPayConfig(
               amount: 2000,
               environment: MyPayEnvironment.dev,
               orderId: "QWE-SEW-SFES-QXZS",
               merchantId: "CAFEQ",
               userName: "username1",
               password: "decryptedPassword",
               returnUrl: "https://mypay.com.np");

```

4. Initialize the payment by calling `MyPay.payment()` method:

```
        MyPay.payment(
        context,
        config: config,
        apiKey: "APIKEY123",
        onSuccess: (MyPaySuccessResult successResponse) {},
        onFailure: (MyPayFailureResult failureResponse) {},
        onCancel: (MyPayCancelResult cancelResponse) {},
        );

```

5. Check the payment result:

Once the payment is finalized, the plugin payment function provides either
an instance of MyPaySuccessResult or MyPayFailureResult based on the outcome.
In the event of a successful payment, the onSuccess parameter is triggered,
allowing the user to execute desired actions. If the user cancels the payment,
the onCancel parameter is triggered, providing an opportunity to take appropriate
actions. Similarly, if the payment fails, the onFailure parameter is triggered,
enabling the user to handle the situation accordingly.

```
        MyPay.payment(
        context,
        config: config,
        apiKey: "APIKEY123",
        onSuccess: (MyPaySuccessResult successResponse) {
           // Perform Server Verification
        },
        onFailure: (MyPayFailureResult failureResponse) {
           // What to do on failure?
        },
        onCancel: (MyPayCancelResult cancelResponse) {
           // User manually cancelled the transaction
        },
        );
```

# Dev/Live Mode

`MyPayConfig` supports both dev and live mode. For live mode, use the `MyPayEnvironment.live`
value in environment parameter in `MyPayConfig`, and for dev mode, use the `MyPayEnvironment.dev`
value in environment parameter in `MyPayConfig`. Here's an example of using the dev mode:

```
final config = MyPayConfig(
               amount: 2000,
               environment: MyPayEnvironment.dev,
               orderId: "QWE-SEW-SFES-QXZS",
               merchantId: "CAFEQ",
               userName: "username1",
               password: "decryptedPassword",
               returnUrl: "https://mypay.com.np");
```

# API

### Class: MyPay

Class provides the main interface for the MyPay payment integration.

### Methods

- `payment(BuildContext context, {required config, required apiKey, ...})`: Initializes the MyPay
  payment gateway with the given configuration. It requires `context`, `myPayConfig`,
  `onSuccess`, `onFailure`, `onCancel`, and `appbar` parameters.

### Properties

- `context` (required): This is the build context of the widget tree.
- `apiKey` (required): This is api key provided my MyPay.
- `onSuccess` (required): This is called when transaction successful.
- `onFailure` (required): This is called when transaction is failed.
- `onCancel` (required): This is called when transaction is cancelled.
- `appbar` (optional): This is the parameter where you can put customized appbar.

### Class: MyPayConfig

The `MyPayConfig` class is used to configure the MyPay payment gateway.

- `MyPayConfig`: used for both live and dev mode configuration. It requires `amount`, `environment`, `userName`,
  `password`, `merchantId`, `orderId`, and `returnUrl` parameters.

### Properties

- `amount` (required): This is the Transaction amount Of the Order which will be debited at the time
  of Payment.
- `environment` (required): This is for setting live or development environment.
- `userName` (required): This is the Merchant UserName provided by MyPay Admin.
- `password` (required): This is the Merchant API Password provided by MyPay Admin.
- `merchantId` (required): This is the Unique Merchant Id provided by MyPay Admin.
- `orderId` (required): This is Order Id of Merchant which will be in numeric string format minimum
  length is greater than equal to 6 chars.
- `returnUrl` (optional): URL of merchant where Transaction Response is expected to be post by
  MyPay. The default returnUrl would be `https://mypay.com.np`

### Class: MyPaySuccessResult

Class representing the success result of a payment transaction.

### Class: MyPayFailureResult

Class representing the failure result of a payment transaction.

### MyPayPayButton

The MyPayPayButton is a customizable button widget that allows users to initiate the payment process
for the MyPay Payment Gateway. The button can be easily integrated into your Flutter app by calling
the MyPayPayButton constructor and passing the required parameters.

Here is an example of how to use the MyPayButton widget:

```
MyPayButton(
  paymentConfig: MyPayConfig(
                 amount: 2000,
                 environment: MyPayEnvironment.dev,
                 orderId: "QWE-SEW-SFES-QXZS",
                 merchantId: "CAFEQ",
                 userName: "username1",
                 password: "decryptedPassword",
                 returnUrl: "https://mypay.com.np"),
  width: 40,
  onSuccess: (MyPaySuccessResult successResponse) {
    // Handle success senerio
   },
  onFailure: (MyPayFailureResult failureResponse) {
    // Handle failure senerio
   },
  onCancel: (MyPayCancelResult cancelResponse) {
    // Handle cancel senerio
   },
),

```

With `MyPayButton`, the integration of MyPay Payment Gateway becomes even easier, as the payment
process can be initiated with just a single button press.

### Properties

The `MyPayButton` widget has the following parameters:

- `width`: The width of the button (default is `double.infinity`).
- `height`: The height of the button (default is `40.0`).
- `color`: The background color of the button (default is the primary color of the app).
- `borderColor`: The border color of the button (default is the primary color of the app).
- `onSuccess`: The callback function that is called when the payment is successful.
- `onFailure`: The callback function that is called when the payment fails.
- `paymentConfig`: The MyPay payment configuration object that is required to initialize the payment
  screen.
- `radius`: The border radius of the button (default is `4`).
- `widget`: An optional widget to be placed instead of the title text field. If the user wants to
  place a row with the MyPay icon or different widget.
- `title`: The title of the button (default is "Pay with MyPay").
- `textStyle`: The text style of the button title.

### Customize appbar

You can customize the appbar of the mypay sdk in the `MyPay.Payment` function by adding parameter appbar and putting the customized appbar on it. You can see customized appbar on example below:

```
MyPayButton(
  appBar: AppBar(
          title: const Text("MyPay Payment"),
          backgroundColor: Colors.green,
          ),
  paymentConfig: MyPayConfig(
                 amount: 2000,
                 environment: MyPayEnvironment.dev,
                 orderId: "QWE-SEW-SFES-QXZS",
                 merchantId: "CAFEQ",
                 userName: "username1",
                 password: "decryptedPassword",
                 returnUrl: "https://mypay.com.np"),
                 ...
)

```

## Dev Testing Information

If you want to test MYPay payment integration in development environment, you can use the following
information:

- `MYPAY_URL`: The URL of the MyPay payment gateway. Use `https://testapi.mypay.com.np/` for
  development environment.
- Test MyPay IDs: You can use any of the following test MyPay IDs for testing purposes:
- Username: 9863535353
- Password: Test@123
- OTP: 123456

## Screenshots

Here are some screenshots of the MyPay Payment Gateway integrated into a ecommerce Flutter app:

<table>
  <tr>
    <td><img src="https://gitlab.com/nischal08/mypay_flutter/-/raw/main/raw/screenshot1.png" alt="Example Order Screen" width="400"/></td>
    <td><img src="https://gitlab.com/nischal08/mypay_flutter/-/raw/main/raw/screenshot2.png" alt="Payment Screen" width="400"/></td>
  </tr>
</table>

## Run the example app

- Navigate to the example folder `cd example`
- Install the dependencies
  - `flutter pub get`
- Set up environment parameter in config `MyConfig(environment:MyPayEnvironment.live)` in live or directly
  run with just `MyConfig(environment:MyPayEnvironment.dev)` in dev mode
- Start the example
  - Terminal : `flutter run`

# License

This plugin is released under the MIT License. See LICENSE for details.

## Contributions

Contributions are welcome! To make this project better, Feel free to open an issue or submit a pull
request on [Github](https://gitlab.com/nischal08/mypay_flutter/issues)..

## Contact

If you have any questions or suggestions, feel free
to [contact me on LinkedIn](https://www.linkedin.com/in/nirajkaranjeet/).
